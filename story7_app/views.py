from django.shortcuts import render
from django.contrib.auth.models import User


def home(request):
    return render(request,
            'story7_app/home.html',
            {
                "is_story7_app" : True,
                "username" : request.user.username
            })
