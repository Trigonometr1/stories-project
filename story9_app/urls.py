from django.urls import path

from . import views

app_name = 'story9_app'

urlpatterns = [
    path("register/", views.register_function, name="register_function"),
    path("login/", views.login_function, name="login_function"),
    path("logout/", views.logout_function, name="logout_function"),
]
