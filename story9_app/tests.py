from django.test import TestCase, Client
from django.contrib.auth.models import User

# Create your tests here.
class loginFunctionTests(TestCase):
    def setUp(self):
        self.new_user = User.objects.create_user("testnewuser", password="passfortesting123")
        self.client = Client()

    def test_url_register(self):
        resp = Client().get('/story9_app/register/')
        self.assertEquals(200, resp.status_code)
    
    def test_url_login(self):
        resp = Client().get('/story9_app/login/')
        self.assertEquals(200, resp.status_code)

    def test_register_login_logout_user(self):
        response = self.client.post('/story9_app/register/', data = {
            "username" : "newuser123",
            "password1" : "samplepass999",
            "password2" : "samplepass999"
        }, follow=True)

        # Kalau setelah valid registrasinya, maka akan balik ke home story7_app
        # Sekaligus melakukan login, tes juga apakah username ditampilkan
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "newuser123")
        self.assertTemplateUsed(response, "story7_app/home.html")

        # tes logout biasa setelah tes di atas
        response = self.client.post('/story9_app/logout/')
        self.assertEquals(response.status_code, 302)

        # tes login setelah tes logout di atas
        # Setelah login, akan balik ke home story7_app
        response = self.client.post('/story9_app/login/', data= {
            "username" : "newuser123",
            "password" : "samplepass999"
        }, follow = True)
        self.assertTemplateUsed(response, "story7_app/home.html")
        self.assertContains(response, "newuser123")

        # tes antara login atau register, pasti akan redirect
        # kalau sudah ada session yang ada
        response = self.client.get('/story9_app/login/')
        self.assertEquals(response.status_code, 302)

        response = self.client.get('/story9_app/register/')
        self.assertEquals(response.status_code, 302)

                # tes logout biasa setelah tes di atas
        response = self.client.post('/story9_app/logout/')
        self.assertEquals(response.status_code, 302)

        # tes register tapi gagal karena sudah ada usernamenya
        response = self.client.post('/story9_app/register/', data = {
            "username" : "testnewuser",
            "password1" : "passfortesting123",
            "password2" : "passfortesting123"
        }, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "A user with that username already exists.")

        # tes login tapi gagal karena belum ada usernamenya
        response = self.client.post('/story9_app/login/', data = {
            "username" : "testnewuserlogin",
            "password" : "passfortesting123"
        }, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "Please enter a correct username and password.")