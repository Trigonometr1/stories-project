from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import login , logout, authenticate
from django.contrib.auth.models import User
from .forms import RegistrationForm, LoginForm, AuthenticationForm
from django.contrib import messages

def register_function(request):
    if request.user.is_authenticated:
        return redirect("story7_app:home")
    context = {
        "register" : RegistrationForm,
        "is_story9_app" : True,
    }

    if request.method == "POST":
        register = RegistrationForm(request.POST)
        if register.is_valid():
            register.save()
            new_user_account = authenticate(
                username = register.cleaned_data["username"],
                password = register.cleaned_data["password1"]
            )
            login(request, new_user_account)
            return redirect("story7_app:home")
        elif register.errors:
            for key in register.errors:
                messages.add_message(request, messages.WARNING, register.errors[key])
    return render(request, "story9_app/register.html", context)

def login_function(request):
    context = {
        "login" : LoginForm,
        "is_story9_app" : True,
    }
    if request.user.is_authenticated:
        return redirect("story7_app:home")
    
    if request.method == "POST":
        login_form = LoginForm(data = request.POST)
        if login_form.is_valid():
            user = login_form.get_user()
            login(request, user)
            return redirect("story7_app:home")
        elif login_form.errors:
            for key in login_form.errors:
                messages.add_message(request, messages.WARNING, login_form.errors[key])
    return render(request, "story9_app/login.html", context)

def logout_function(request):
    if request.user.is_authenticated:
        logout(request)
    return redirect("story7_app:home")