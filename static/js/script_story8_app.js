$(document).ready(function() {
    // Munculkan array of JSON buku setelah klik tombol
    $("#find-btn").click(() => {
        // Ambil array of JSON buku yang ada.
        var keyword = $("#book-form").val();
        if (keyword == "") {
            alert("Pencarian gagal. Mohon masukkan judul buku yang ingin dicari.")
        } else {
            var apiUrl = "data/?q=" + keyword;
            //console.log(apiUrl);
            $.ajax({
                url: apiUrl,
                success: function(JsonObject) {
                    $("#book-list").empty() // Kosongkan agar tersedia ruang untuk meletakkan JSON nanti.
                    // Tambahkan keterangan jumlah buku yang bisa ditemukan
                    $("#found-amount").text(JsonObject.items.length)
                    for (i = 0; i < JsonObject.items.length; i++) {
                        var bookList = $("#book-list");
                        // console.log(JsonObject.items[i]);
                        // Coba ambil image dan tanggal publikasinya. Kalau undefined, kembalikan foto lain
                        // Ada 2 versi. Bisa try catch atau pake syntax tanda tanya
                        try {
                            var thumbnailLink = JsonObject.items[i].volumeInfo.imageLinks.thumbnail;
                        } catch {
                            var thumbnailLink = "https://upload.wikimedia.org/wikipedia/commons/f/fc/No_picture_available.png"
                        }
                        var tanggalPublikasi = JsonObject.items[i].volumeInfo.publishedDate ? JsonObject.items[i].volumeInfo.publishedDate : "Tidak diketahui"
                        var deskripsi = JsonObject.items[i].volumeInfo.description ? JsonObject.items[i].volumeInfo.description : "Tidak ada deskripsi untuk buku ini"
                        // Tambahkan hardcoded HTML yang akan diappend langsung.
                        bookList.append(
                            `<div class="card w-75 border-dark m-3">
                                <h3 class="card-header bg-title-card text-center text-left-lg">${JsonObject.items[i].volumeInfo.title}</h3>
                                <div class="card-body bg-content-card">
                                    <div class="row">
                                        <div class="col-lg-3 col-12 d-flex align-items-center justify-content-center">
                                            <img class="mr-3 book-cover" src="${thumbnailLink}">
                                        </div>
                                        <div class="col-lg-9 col-12 book-info">
                                            <h5 class="card-title">Published on ${tanggalPublikasi}</h5>
                                            <p class="card-text text-justify">${deskripsi}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>`
                        )
                    }
                }
            })
        }
    })
})