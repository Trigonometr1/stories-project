$(document).ready(function() {
    $("#accordion").accordion(); // Buat accordionnya

    function sortByPosition(a, b) {
        // Ambil position attribute di HTML, kemudian ubah ke angka
        var posItem1 = Number($(a).attr("position"));
        var posItem2 = Number($(b).attr("position"));

        if(posItem1 < posItem2) return -1;
        if(posItem2 > posItem1) return 1;
        return 0;
    }

    function sortAccordion(clickedH3, operation) {
        var elementList = $("#accordion").children();
        var accordionSorted = $("#accordion");
        
        // index element yang diklik di clickedPos
        // Isinya ada di clickedContent
        var clickedPos = elementList.index(clickedH3);
        var clickedContent = elementList[clickedPos+1];

        // Kalau sudah di paling atas, jangan lakukan apapun
        // Begitu juga kalau sudah paling bawah
        if(clickedPos == 0 && operation == "up") return
        if(clickedPos == elementList.length - 1 && operation == "down") return

        if(operation == "up") {
            // Kalau up, maka akan terjadi tukar posisi
            // antara clickedContent dengan konten diatasnya

            var aboveH3 = $(elementList[clickedPos-2]);
            console.log(aboveH3)
            var aboveContent = $(elementList[clickedPos-1]);
            console.log(aboveContent)

            // Posisi dan konten saat ini nilai positionnya ditukar
            // dengan yang ada di atas

            $(clickedH3).attr("position", aboveH3.attr("position"))
            $(clickedContent).attr("position", aboveContent.attr("position"))

            // Yang diatas, posisinya bertambah satu untuk konten dan h3
            aboveH3.attr("position", aboveH3.attr("position") + 1)
            aboveContent.attr("position", aboveContent.attr("position") + 1)

        } else if(operation == "down") {
            // Kalau down, maka akan terjadi tukar posisi
            // antara clickedContent dengan konten dibawahnya
            var belowH3 = $(elementList[clickedPos+2]);
            var belowContent = $(elementList[clickedPos+3]);

            // Posisi dan konten saat ini nilai positionnya ditukar
            // dengan yang ada di bawah

            $(clickedH3).attr("position", belowH3.attr("position"))
            $(clickedContent).attr("position", belowContent.attr("position"))

            // Yang dibawah, posisinya berkurang satu karena akan geser keatas
            belowH3.attr("position", belowH3.attr("position") - 1)
            belowContent.attr("position", belowContent.attr("position") - 1)

        }

        // Atur lagi urutan di elementList karena sudah ada element yang
        // Posisinya berubah

        elementList.sort(sortByPosition);

        // Value disini adalah tiap element di HTML
        $.each(elementList, (key, value) => {
            accordionSorted.append(value);
        })
    }

    $(".up").click((e) => {
        var clickedContent = e.target.parentElement;
        sortAccordion(clickedContent, "up")
    })

    $(".down").click((e) => {
        var clickedContent = e.target.parentElement;
        sortAccordion(clickedContent, "down")
    })
})