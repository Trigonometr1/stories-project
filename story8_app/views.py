from django.shortcuts import render, redirect
from django.http import JsonResponse
import requests
import json
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required(login_url = "/story9_app/login")
def home(request):
    context = {
        "is_story8_app" : True,
        "username" : request.user.username
    }
    return render(request, "story8_app/home.html", context)

def data_function(request):
    arg = request.GET["q"]
    api_url = "https://www.googleapis.com/books/v1/volumes?q=" + arg
    response = requests.get(api_url)
    data = json.loads(response.content)
    return JsonResponse(data, safe=False)