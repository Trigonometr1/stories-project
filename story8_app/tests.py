from django.test import TestCase, Client
from django.urls import reverse, resolve
from story8_app.views import home
from django.contrib.auth.models import User

# Create your tests here.
class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
    
    def test_home_GET(self):
        self.home_url = resolve(reverse("story8_app:home"))
        self.assertEquals(self.home_url.func, home)

    def test_home_after_authentication(self):
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        self.home_url = reverse("story8_app:home")
        response = self.client.get(self.home_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "story8_app/home.html")


    
    def test_data_function(self):
        response = self.client.get(reverse('story8_app:data_function') + '?q=Tes')
        # url yg dipanggil lewat GET client jadinya /story8_app/data/?q=Tes
        self.assertEquals(response.status_code, 200)
        self.assertIn('Tes', response.content.decode("utf-8"))