from django.urls import path

from . import views

app_name = 'story8_app'

urlpatterns = [
    path('', views.home, name='home'),
    path('data/', views.data_function, name='data_function'),
]